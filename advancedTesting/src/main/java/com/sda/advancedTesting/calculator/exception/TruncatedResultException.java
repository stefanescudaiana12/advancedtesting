package com.sda.advancedTesting.calculator.exception;

public class TruncatedResultException extends RuntimeException{
    public TruncatedResultException(String message) {
        super(message);
    }
}
